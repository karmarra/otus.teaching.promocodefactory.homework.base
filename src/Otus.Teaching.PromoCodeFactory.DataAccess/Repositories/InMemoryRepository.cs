﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {

        public InMemoryRepository(IEnumerable<T> data)
        {
            if (Storage<T>.Data == null) 
                Storage<T>.Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Storage<T>.Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Storage<T>.Data?.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> PostAsync(T entity)
        {
            if (Storage<T>.Data == null)
                Storage<T>.Data = new List<T>();

            entity.Id = Guid.NewGuid();

            Storage<T>.Data = Storage<T>.Data.Append(entity);
            return Task.FromResult(entity);
        }

        public Task PatchAsync(Guid id, T entity)
        {
            if (Storage<T>.Data?.Any(x => x.Id == id) == true)
                Storage<T>.Data = Storage<T>.Data.Select(x => x.Id == id ? entity : x).ToList();

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            if (Storage<T>.Data?.Any(x => x.Id == id) == true)
                Storage<T>.Data = Storage<T>.Data.Where(x => x.Id != id).ToList();

            return Task.CompletedTask;
        }
    }
}