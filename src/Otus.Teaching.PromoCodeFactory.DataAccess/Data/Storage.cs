﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class Storage<T> where T : BaseEntity
    {
        public static IEnumerable<T> Data { get; set; }
    }
}