﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public string Email { get; set; }
    }
}